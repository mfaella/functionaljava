import java.util.Arrays;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamIntro {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Employee[] emps = {
				new Employee("Mike", 2500),
				new Employee("Frank", 3000),
				new Employee("Hannah", 2500),
				new Employee("Rajeev", 2000),
				new Employee("Jessica", 1500),
				new Employee("Doug", 2000),
				new Employee("Chen", 3500),
				new Employee("Krish", 2200)
			};
		Arrays.stream(emps).filter(e -> e.getSalary()>=2500)
						   .map(Employee::getName)
						   .sorted()
						   .forEach(System.out::println);
		
		Stream<Employee> s1 = Stream.of(emps);
		Stream<Integer> s2 = Stream.of(1, 2, 4);
		Stream<String> s3 = Stream.of("uno", "due", null);
		Stream<String> s4 = Stream.of(new String[] {"uno", "due", null});
		
		// s2.forEach(System.out::println);
		Stream<Integer> s2part = s2.limit(2);
		s2part.forEach(System.out::println);
		
		final Random random = new Random();
		Stream<Integer> randoms = Stream.generate(random::nextInt);
		randoms.limit(10).forEach(System.out::println);
		Stream<String> as = Stream.iterate("a", s -> s + "a");	

		// On suspending streams
		Supplier<Integer> supp = () -> {
			System.out.println("(supplying)");
			return random.nextInt();
		};
		System.out.println("\n Test 3");
		Stream<Integer> randoms3 = Stream.generate(supp);
		randoms3.limit(3).filter(n -> n>=0);
		System.out.println("so far, so good");
		randoms3.forEach(System.out::println);

	}

}
