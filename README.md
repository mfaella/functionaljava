# Functional Programming with Streams in Java 9 #

This is the repository for the Packt course on functional programming with Java streams.
Refer to the course for more information on the files.

[Go to the course page](https://www.packtpub.com/application-development/functional-programming-streams-java-9-video)