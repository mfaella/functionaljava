import java.util.Random;
import java.util.stream.Stream;

public class Employee {
	private String name;
	private int salary;
	
	public Employee(String name, int salary) {
		this.name = name;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	@Override
	public String toString() {
		return name + ": " + salary;
	}
	
	private static final Random rand = new Random();
	
	/*
	 * Returns an array of n employees with random salaries.
	 */
	public static Employee[] randomlyGenerate(int n) {
		return Stream.generate(() -> {
			final int MAX_SALARY = 10_000;
			String name = "John Doe";
			/* Here is a (very inefficient) way to generate
			 * a random name, with streams:
			 * 
			 * final int NAMELENGTH = 5;
			 * String name = rand.ints(NAMELENGTH, 0, 25).mapToObj(i -> "" + (char)(61+i)).collect(Collectors.joining()); 
			 */
			return new Employee(name, rand.nextInt(MAX_SALARY));
		}).limit(n).toArray(Employee[]::new);
	}
}
