import java.util.Arrays;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Stream;

import collisions.Time;


public class Parallel {

	public static void main(String[] args) {
		testEmployees();		
	}

	public static void testEmployees() {
		long total;
		Employee[] emps = Employee.randomlyGenerate(50_000_000);


		System.out.println("\nSequential imperative");

		// Warm-up
		total = getTotalSalaryImperative(emps);
		// Now I measure it
		total = Time.timeIt(Parallel::getTotalSalaryImperative, emps);
		System.out.println("Total salary: " + total);


		System.out.println("\nParallel imperative, no synchronization");

		total = getTotalSalaryImperativeParallel(emps);
		total = Time.timeIt(Parallel::getTotalSalaryImperativeParallel, emps);
		System.out.println("Total salary: " + total);
		total = Time.timeIt(Parallel::getTotalSalaryImperativeParallel, emps);
		System.out.println("Total salary: " + total);
		total = Time.timeIt(Parallel::getTotalSalaryImperativeParallel, emps);
		System.out.println("Total salary: " + total);


		System.out.println("\nParallel imperative, synch");

		total = getTotalSalaryImperativeSynch(emps);
		total = Time.timeIt(Parallel::getTotalSalaryImperativeSynch, emps);
		System.out.println("Total salary: " + total);


		System.out.println("\nParallel imperative, w. LongAdder");
		total = getTotalSalaryImperativeLongAdder(emps);
		total = Time.timeIt(Parallel::getTotalSalaryImperativeLongAdder, emps);
		System.out.println("Total salary: " + total);


		System.out.println("\nStateless sequential stream");
		// Warm-up
		total = getTotalSalarySequential(emps);
		// Now we measure it
		total = Time.timeIt(Parallel::getTotalSalarySequential, emps);
		System.out.println("Total salary: " + total);


		System.out.println("\nStateless parallel stream");
		total = getTotalSalaryParallel(emps);
		total = Time.timeIt(Parallel::getTotalSalaryParallel, emps);
		System.out.println("Total salary: " + total);
	}
	
	public static long getTotalSalarySequential(Employee[] emps) {
		return Arrays.stream(emps)
			         .mapToLong(emp -> emp.getSalary())
			         .sum();			
	}
	
	public static long getTotalSalaryParallel(Employee[] emps) {
		return Arrays.stream(emps)
				     .parallel()
			         .mapToLong(Employee::getSalary)
			         .sum();			
	}

	
	interface SalaryAdder {
		void accept(Employee e);
		long getTotal();			
	}

	static class SalaryAdderPlain implements SalaryAdder {
		long total;
		
		public void accept(Employee e) {
			total += e.getSalary();
		}
		public long getTotal() { return total; }
	}

	static class SalaryAdderSynch implements SalaryAdder {
		long total;
		
		public synchronized void accept(Employee e) {
			total += e.getSalary();
		}
		public long getTotal() { return total; }
	}
	
	static class SalaryAdderLongAdder implements SalaryAdder {
		LongAdder total = new LongAdder();
		
		public void accept(Employee e) {
			total.add(e.getSalary());
		}
		public long getTotal() { return total.longValue(); }
	}

	public static long getTotalSalaryImperative(Employee[] emps) {
		SalaryAdder adder = new SalaryAdderPlain();
		Arrays.stream(emps).forEach(adder::accept);
		return adder.getTotal();
	}

	public static long getTotalSalaryImperativeParallel(Employee[] emps) {
		SalaryAdder adder = new SalaryAdderPlain();
		Arrays.stream(emps).parallel().forEach(adder::accept);
		return adder.getTotal();
	}
	
	public static long getTotalSalaryImperativeSynch(Employee[] emps) {
		SalaryAdder adder = new SalaryAdderSynch();
		Arrays.stream(emps).parallel().forEach(adder::accept);
		return adder.getTotal();
	}
	
	public static long getTotalSalaryImperativeLongAdder(Employee[] emps) {
		SalaryAdder adder = new SalaryAdderLongAdder();
		Arrays.stream(emps).parallel().forEach(adder::accept);
		return adder.getTotal();
	}
}
