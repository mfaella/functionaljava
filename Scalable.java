public interface Scalable {
	
	// Implicitly public abstract
	void setScale(double scale);

	// Implicitly public static final
	double DEFAULT_SCALE = 1.0;

	// New possibilities from Java 8:
	
	// Implicitly public
	static boolean isScalable(Object obj) {
		return obj instanceof Scalable;
	}
	
	// Implicitly public
	default void resetScale() {
		setScale(DEFAULT_SCALE);
	}
}
