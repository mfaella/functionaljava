import java.util.function.*;

public class LambdaInference {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		// *** Part 1: Identify the FI

		// Standard syntax
		Consumer<String> c1 = msg -> System.out.println(msg.length());
		

		// Compile-time error: not enough info
		// Object x1 = msg -> System.out.println(msg.length());
				
		
		// Compile-time error: not enough info
		// Object x2 = (String msg) -> System.out.println(msg.length());
		
		
		// OK: cast added
		Object x3 = (Consumer<String>) ((msg) -> System.out.println(msg.length()));

		
		
		
		
		
		
		
		
		
		
		
		
		
		// *** Part 2: Identify the parameter types

		
		// OK: but inferred parameter type is Object; lambda exp targets Consumer<Object>
		Consumer<?> c2 = msg -> System.out.println(msg);
		
		
		// Compile-time error: Inference is *not* based on body of lambda
		// Consumer<?> c3 = msg -> System.out.println(msg.length());
		
		
		// OK: added manifest type to parameter: lambda exp targets Consumer<String>
		Consumer<?> c4 = (String msg) -> System.out.println(msg.length());

		// OK: inference based on receiving context
		Consumer<String> c5 = msg -> System.out.println(msg.length());
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static <T extends Employee> T testWithNull(UnaryOperator<T> transformer) {
		return transformer.apply(null);
	}
}
